class FizzBuzz:
    """Implementation of fizzbuzz where the user supplies the parameters."""
    
    def __init__(self, max_number):
        if max_number < 1:
            raise IndexError("Maximum number has to be greater than zero")
        self.max_number = max_number
        
    def fizzbuzz(self):
        """Prints numbers in range 1 to max_number replacing every multiple of 3 with "Fizz" and of 5 with "Buzz"."""
        
        for i in range(1,self.max_number + 1):
            if (i%3 == 0) and (i%5 == 0):
                print "FizzBuzz"
            elif (i%3 == 0):
                print "Fizz"
            elif (i%5 == 0):
                print "Buzz"
            else:
                print i
                
if __name__ == '__main__':
    fizz = FizzBuzz(20)
    fizz.fizzbuzz()
    
