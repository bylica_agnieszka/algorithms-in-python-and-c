# Agnieszka Bylica

class Rod:
    """Class implements solution of Rod Cutting Problem with reading data from file - dynamic programming."""
    
    def __init__(self, filename):
        self.price_list_file = open(filename,'r')
        self.price_list = dict() # price_list[length] = price
        self.rod_size = int(raw_input("Enter rod's length:"))
        for line in self.price_list_file:
            line = line.replace('\n','')
            line = line.split(' ')
            self.price_list[int(line[0])] = int(line[1])
        self.price_list_file.close()
        
    def max_revenue(self):
        """Method return maximum revenue obtainable by cutting up the rod and selling the pieces."""
        revenue = [0]
        for j in range(1,self.rod_size +1):
            q = -1
            for i in range(1,j+1):
                q = max(q,self.price_list[i] + revenue[j-i])
            revenue.insert(j,q)
        return revenue[self.rod_size]
        
rod = Rod("price_list.txt")
print rod.max_revenue()
        
