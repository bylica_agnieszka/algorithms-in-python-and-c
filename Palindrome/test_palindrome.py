from palindrome import Palindrome
import pytest

def test_value_erron_on_empty_string():
    with pytest.raises(ValueError):
        Palindrome("")
        
def test_value_erron_if_not_string():
    with pytest.raises(ValueError):
        Palindrome(10)
        
def test_is_palindrome_one():
    single = Palindrome("a")
    assert single.is_palindrome() == True
    
def test_is_palindrome_two():
    double = Palindrome("rr")
    assert double.is_palindrome() == True
    
def test_is_palindrome():
    pal = Palindrome("allklla")
    assert pal.is_palindrome() == True

def test_is_not_palindrome_two():
    double = Palindrome("ra")
    assert double.is_palindrome() == False
    
def test_is_not_palindrome_three():
    triple = Palindrome("rak")
    assert triple.is_palindrome() == False
    
def test_is_not_palindrome():
    pal = Palindrome("rakhar")
    assert pal.is_palindrome() == False
