class Palindrome:
    """Class contains methods to check if string is palindrome"""
    def __init__(self,user_string):
        if not isinstance(user_string,str):
            raise ValueError("Argument is not a string")
        if len(user_string) == 0:
            raise ValueError("User's string is empty")
        self.user_string = user_string
        self.length = len(self.user_string)
        
    def is_palindrome(self):
        """Returns True if user_string is palindrome"""
        for index in range( (self.length//2)):
            if self.user_string[index] != self.user_string[self.length - index - 1]:
                return False
        return True
    
pal = Palindrome("racecar")
print(pal.is_palindrome())
