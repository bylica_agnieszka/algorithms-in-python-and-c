class AlarmClock:
    """ Class contains simple alarm clock """
    
    def __init__(self,sound_directory,time):
        if not isinstance(sound_directory,str):
            raise ValueError("Format of sound file directory is wrong")
        if not isinstance(time,str):
            raise ValueError("Time is not string")
        self.sound_directory = sound_directory
        self.time = time
        
    def alarm_clock(self):
        """ Method checks current time and calls alarm() when a certain time is reached"""
        from datetime import datetime
        import time
        while(True):
            act_time = str(datetime.now().time())
            act_time = act_time[0:5]
            if act_time == self.time:
                self.alarm()
                break
            time.sleep(30)
        
    def alarm(self):
        """ Method plays a sound for 20 seconds"""
        from pygame import mixer
        import time
        mixer.init()
        mixer.music.load(self.sound_directory)
        mixer.music.play(-1,0.0)
        time.sleep(20)
