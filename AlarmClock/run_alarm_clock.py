from alarm_clock import AlarmClock

set_time = raw_input("Give the time in format hour:minutes\n")
try:
    myAlarm1 = AlarmClock("Cool-alarm-tone-notification-sound.mp3",set_time)
except ValueError as value:
    print("Initialization failed with {}".format(value))
myAlarm1.alarm_clock()
