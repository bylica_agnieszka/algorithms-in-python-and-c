from alarm_clock import AlarmClock
import pytest

@pytest.fixture
def sound_directory_not_string():
    return AlarmClock(9,"22:00")

@pytest.fixture
def time_not_string():
    return AlarmClock("",True)

@pytest.fixture
def correct_init_object():
    return AlarmClock("Cool-alarm-tone-notification-sound.mp3","22:00")

def test_init():
    with pytest.raises(ValueError):
        sound_directory_not_string()
    with pytest.raises(ValueError):
        time_not_string()
        

    
    
