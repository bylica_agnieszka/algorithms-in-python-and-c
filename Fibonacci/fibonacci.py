# Wyrazy ciagu Fibonacciego przy pomocy programowania dynamicznego. Agnieszka Bylica

def fibonacci(n):
    """Funkcja oblicza n-ty wyraz ciagu Fibonacciego przy pomocy programowania dynamicznego"""
    if n < 0:
        raise ValueError( "Fibonacci number should be positive integer.")

    Fibonacci = [0] + n*[1]
    for i in range(2,n+1):
        Fibonacci[i] = Fibonacci[i-1] + Fibonacci[i-2]
    return Fibonacci[n]
    
f = int(raw_input("Enter positive integer number:"))
print "F" + str(f) + " = " + str(fibonacci(f))

    
