// Wyrazy ciagu Fibonacciego przy pomocy programowania dynamicznego. Agnieszka Bylica
# include <iostream>
# include <limits>

using namespace std;

int fibonacci ( int n ) {
   // Funkcja oblicza n-ty wyraz ciagu Fibonacciego przy pomocy programowania dynamicznego
    if (n < 0)
    {
        cout<<"Fibonacci number should be positive integer."<<endl;
        return -1;
    }
    if (n == 0)
        return 0;
    if (n == 1)
        return 1;
    
    int * Fibonacci = new int[n+1];
    Fibonacci[0] = 0;
    Fibonacci[1] = 1;
    
    for ( int i = 2; i <= n; i++ )
    {
        Fibonacci[i] = Fibonacci[i-1] + Fibonacci[i-2];
    }
    return Fibonacci[n];
    
}

int main(){
    
    int f = 0;
    cout<<"Enter positive integer number:"<<endl;
    while(true)
    {
        cin>>f;
        if(cin.fail())
        {
            cout<<"Fibonacci number should be positive integer."<<endl;
            cin.clear();
            cin.ignore( numeric_limits<streamsize>::max(), '\n');
        }
        else break;
    }
    cout<<"F"<<f<<" = "<<fibonacci(f)<<endl;

}
